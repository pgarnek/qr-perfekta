
var pathArray = window.location.pathname.split( '/' );
var base_url = window.location.protocol + "//" + window.location.host + "/" + pathArray[1] + "/" /*+ pathArray[2] + "/"*/;

function zero_wiodace(zmienna)
{
    if(zmienna < 10)
        {
           return '0' + zmienna; 
        }
    else
        {
            return zmienna;
        }    
}

function jestLiczba(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

// własny timestamp w JS
function data_ts() {
    var data = new Date();
    var rok = data.getFullYear();
    var mies = data.getMonth();
    var dzien = data.getDate();
    var godz = data.getHours();
    var min = data.getMinutes();
    var sec = data.getSeconds();
    // Dodanie zera na początku minut i sekund jeżeli trzeba

    var data_i_czas = rok + "-" + zero_wiodace(mies) + "-" + zero_wiodace(dzien) + 
            " " + zero_wiodace(godz) + ":" + zero_wiodace(mies) + ":" + zero_wiodace(sec) + "";
    return data_i_czas;
}

function Stos()
{
    this.rozmiar = 0;
    this.stac = new Array();
    this.wyjmij = function() {
        return this.stac.pop();
        --this.rozmiar;
    };
    this.wrzuc = function(item) {
        this.stac.push(item);
        ++this.rozmiar;
    };
};


$(document).ready( function()

{
    // spr. czy to nie jest edycja (wtedy przycisk zapisz ZAWSZE WIDOCZNY
    if( $("#form_action").val() == 'save_edit' || $("#form_action").val() == 'edit' ) {
        $('#zapisz_kod_qr').show();
    }

    $("#nowy_qr").submit( function() { //łapie FORMULARZA, nie przycisk

        $.ajax({   
            url: base_url + 'home/ajax_generuj2', 
            async: false,
            type: "POST", 
            data: $(this).serializeArray(), //The variables which are going.
            dataType: "html", //Return data type (what we expect).

            success: function(data) {
                $('#wygenerowany_kod').html(data);
                $('#zapisz_kod_qr').show();
                $('#zapisz_nowy_kod_qr').show();
            }
        });
        return false;
    });
    
    $("#zapisz_kod_qr").click( function() {
        $.ajax({   
            url: base_url + 'home/ajax_zapisz', //The url where the server req would we made.
            async: false,
            type: "POST", //The type which you want to use: GET/POST
            data: $("#nowy_qr").serializeArray(), //The variables which are going.
            dataType: "html", //Return data type (what we expect).

            //This is the function which will be called if ajax call is successful.
            success: function(data) {
                //data is the html of the page where the request is made.
                if(data != null) {
                    window.location = data;
                }
                else {
                    $('#stany').html('Nie zapisano... :-(');
                }   
            }
        });
        return false;
    });

    $("#zapisz_nowy_kod_qr").click( function() {
        $("#form_action").val("");
        $("#qr_id").val("");
        $("#utworzono").val(data_ts());

        $.ajax({   
            url: base_url + 'home/ajax_zapisz', //The url where the server req would we made.
            async: false,
            type: "POST", //The type which you want to use: GET/POST
            data: $("#nowy_qr").serializeArray(), //The variables which are going.
            dataType: "html", //Return data type (what we expect).

            //This is the function which will be called if ajax call is successful.
            success: function(data) {
                //data is the html of the page where the request is made.
                if(data != null) {
                    window.location = data;
                }
                else {
                    $('#stany').html('Nie zapisano... :-(');
                }   
            }
        });
        return false;
    });



     $("#formularz_szablon").submit( function() { //łapie FORMULARZA, nie przycisk
        $.ajax({   
            url: base_url + 'home/ajax_zapisz_szablon', //The url where the server req would we made.
            async: false,
            type: "POST", //The type which you want to use: GET/POST
            data: $("#formularz_szablon").serializeArray(), //The variables which are going.
            dataType: "html", //Return data type (what we expect).

            //This is the function which will be called if ajax call is successful.
            success: function(data) {
                //data is the html of the page where the request is made.
                if(data != null) {
                    window.location = data;
                }
                else {
                    $('#stany').html('Nie zapisano... :-(');
                }   
            }
        });
        return false;
    });
    
/*
 * FUNKCJA ODPOWIEDZIALNA ZA WYŚWIETLANIE KODU QR Z WYBRANYM SZABLONEM, 
 * 
 * po wcisnieciu przycisku "WYBIERZ".
 * funkcja ma po wybraniu wyświetlić kod z adresem w którym jest ID kodu i nowego szablonu.
 * po wyświetleniu, ma NADPISAĆ szablon_id tym, który jest w podglądzie
 * 
 */

    $("#wybierz_szablon").click(function() {
        var url = $(location).attr('href'); // cały adres url
        var szukane = 'view';               // hasło szukane w adresie
        var szukanePos = url.search(szukane) + szukane.length;  // pozycja za słowen "view"
        var urlParts = url.substring(szukanePos).split("/");    // pozostały adres dzieli, pierwsza część to ID kodu
        var wybranySzablon = $("#lista_szablonow option:selected").val();
        if(wybranySzablon == 0) wybranySzablon = "";
        
        var newUrl = url.substring(0, szukanePos) + "/" + urlParts[1] + "/" + wybranySzablon;
        
        if(newUrl != url) window.location = newUrl;
    });
    
/*
 * funkcja dotyczy powyższej - po wczytaniu szablonu pod nowym URL, trzeba zmienić atrybut z listy szablonów na ten wybrany
 * 
 */
    if(pathArray[6]) $("#lista_szablonow").val(pathArray[6]);
    
});




