<?php if($lista):?>
<table class="lista lista_kodow">
    <thead>
        <tr>
            <td>osoba</td>
            <td>firma</td>
            <td>kom.</td>
            <td>e-mail</td>
            <td>ost. modyfikacja</td>
            <td>A</td>
        </tr>
    </thead>
    <tbody>
        <?php foreach($lista as $k):?>
        <tr id="<?php echo $k->qr_id;?>" class="qr_table_row">
            <td><?php echo anchor('home/view/'.$k->qr_id, $k->nazwisko." ".$k->imie, array('class' => 'qr_link'));?></td>
            <td class="center"><?php echo $k->firma;?></td>
            <td class="center" title="<?php echo $k->komorkowy;?>">
                <?php if($k->komorkowy) echo img('images/icons/action_check.png'); ?>
            </td>
            <td class="center" title="<?php echo $k->email;?>">
                <?php if($k->email) echo img('images/icons/action_check.png'); ?>
            </td>
            <td class="center" title="<?php echo $k->zmodyfikowano;?>"> <?php echo date('d M Y', strtotime($k->zmodyfikowano)); ?> </td>
            <td class="center"><?php echo img('images/icons/action_delete.png');?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="6">
                <?php echo $paginacja;?>
            </td>
        </tr>
    </tfoot>
</table>
<?php endif; ?>
