<h2>Uzupełnij Dane</h2>
<form id="nowy_kod" method="POST">
    <input name="imie"        placeholder="Imię" required>
    <input name="nazwisko"    placeholder="Nazwisko" required>
    <br /><br />
    <input name="firma"       placeholder="Firma">
    <input name="stanowisko"  placeholder="Stanowisko">
    <br /><br />
    <input name="kod_pocztowy" placeholder="Kod pocztowy">
    <input name="miasto"      placeholder="Miejscowość">
    <input name="ulica"       placeholder="Adres (ulica, nr)">
    <input name="kraj"        placeholder="Kraj">
    <br /><br />
    <input name="stacjonarny" placeholder="Telefon stacjonarny" >
    <input name="fax"         placeholder="Fax">
    <input name="komorkowy"     placeholder="Telefon komórkowy">
    <br /><br />
    <input name="url"         placeholder="Adres strony www">
    <input name="email"       placeholder="Adres e-mail">
    <br /><br /><br />

    <input type="submit" value="Generuj" id="generuj_kod_qr">
    <button id="zapisz_kod_qr">Zapisz kod QR w bazie</button>
</form>
<div id="wygenerowany_kod" class="margin_top_50">&nbsp;</div>