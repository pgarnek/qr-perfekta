<!doctype html>

<html>

  <head>
        <meta charset="utf-8" />
        <?php foreach(@$styl as $css) echo "\n\t\t".'<link rel="stylesheet" href="'.$css.'">'; ?>
        <?php foreach(@$skrypt as $js) echo "\n\t\t".'<script src="'.$js.'"></script>'; ?>
        
        <title>Generator kodów QR</title>
  </head>

  <body>
      <div class="container_24" id="content">
          <div class="grid_7" id="panel_left">
              <div class="menu_anchor margin_top_50">
                  <a href="<?php echo site_url('home/create') ?>">Nowy kod QR</a>
              </div>
              <div class="menu_anchor" id="home_link">
                  <a href="<?php echo site_url('home') ?>">Lista kodów QR</a>
              </div>
              <div class="menu_anchor margin_top_50">
                  <a href="<?php echo site_url('home/szablony') ?>">WZORCE<br />(Szablony kodów)</a>
              </div>
              <div id="statystyka">
                  <h3>Statystyka</h3>
                  Liczba kodów w bazie: <strong><?php   echo $ile_kodow;        ?></strong><br />
                  Liczba wzorców w bazie: <strong><?php echo $ile_szablonow;    ?></strong>
              </div>
              <div id="ostatnio_dodane" class="margin_top_50">
                  <h6>Ostatnio dodane:</h6>
                  <ul>
                      <?php foreach($ostatnio_dodane as $od):?>
                      <li><?php echo anchor('home/view/'.$od['id'], $od['nazwa']);?></li>
                      <?php endforeach;?>
                  </ul>
              </div>
          </div>
          <div class="grid_13 prefix_1 suffix_1" id="panel_main">
              <div id="main_content">