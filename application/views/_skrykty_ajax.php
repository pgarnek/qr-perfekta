<script>
$(document).ready( function()

{
    // spr. czy to nie jest edycja (wtedy przycisk zapisz ZAWSZE WIDOCZNY
    if( $("#form_action").val() == 'save_edit' || $("#form_action").val() == 'edit' ) {
        $('#zapisz_kod_qr').show();
    }

    $("#nowy_qr").submit( function() { //łapie FORMULARZA, nie przycisk

        $.ajax({   
            url: "<?php echo site_url('home/ajax_generuj') ?>", //The url where the server req would we made.
            async: false,
            type: "POST", //The type which you want to use: GET/POST
            data: $(this).serializeArray(), //The variables which are going.
            dataType: "html", //Return data type (what we expect).

            //This is the function which will be called if ajax call is successful.
            success: function(data) {
                //data is the html of the page where the request is made.
                $('#wygenerowany_kod').html(data);
                $('#zapisz_kod_qr').show();
            }
        });
        return false;
    });
    
    $("#zapisz_kod_qr").click( function() {
        $.ajax({   
            url: "<?php echo site_url('home/ajax_zapisz') ?>", //The url where the server req would we made.
            async: false,
            type: "POST", //The type which you want to use: GET/POST
            data: $("#nowy_qr").serializeArray(), //The variables which are going.
            dataType: "html", //Return data type (what we expect).

            //This is the function which will be called if ajax call is successful.
            success: function(data) {
                //data is the html of the page where the request is made.
                if(data != null) {
                    window.location = data;
                }
                else {
                    $('#stany').html('Nie zapisano... :-(');
                }   
            }
        });
        return false;
    });



     $("#formularz_szablon").submit( function() { //łapie FORMULARZA, nie przycisk
        $.ajax({   
            url: "<?php echo site_url('home/ajax_zapisz_szablon') ?>", //The url where the server req would we made.
            async: false,
            type: "POST", //The type which you want to use: GET/POST
            data: $("#formularz_szablon").serializeArray(), //The variables which are going.
            dataType: "html", //Return data type (what we expect).

            //This is the function which will be called if ajax call is successful.
            success: function(data) {
                //data is the html of the page where the request is made.
                if(data != null) {
                    window.location = data;
                }
                else {
                    $('#stany').html('Nie zapisano... :-(');
                }   
            }
        });
        return false;
    });
});
</script>