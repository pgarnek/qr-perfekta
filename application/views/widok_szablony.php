<?php if($lista):?>
<table class="lista lista_szablonow">
    <thead>
        <tr>
            <td>Nazwa szablonu</td>
            <td>Firma</td>
            <td>Utworzono</td>
            <td>Ostatnia modyfikacja</td>
        </tr>
    </thead>
    <tbody>
        <?php foreach($lista as $l): ?>
        <tr>
            <td><?php echo anchor('home/szablon/'.$l->szablon_id, $l->nazwa); ?></td>
            <td><?php echo $l->firma; ?></td>
            <td><?php echo $l->utworzono; ?></td>
            <td><?php echo $l->zmodyfikowano; ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php endif; ?>
    
<?php echo anchor('home/szablon', 'Dodaj nowy szablon', array('id' => 'przycisk_dodaj_szablon')); ?>
