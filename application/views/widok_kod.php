<h2>Uzupełnij Dane wizytówki</h2>
<form class="formularz" id="nowy_qr" method="POST">
    <table class="styled_table">
        <tr>
            <td class="label">Szablon</td>
            <td>
                <?php echo form_dropdown('szablon_id', $szablony_radio, @$qr->szablon_id, ' id="lista_szablonow"'); ?>
                &nbsp;<a id="wybierz_szablon">wybierz</a>
            </td>
        </tr>
        
        <?php /*  $label  $name  $value  $class  $required  $disabled  $hi_variable   */
            echo table_row_separator();
            echo table_row_generate( "Imię", 'imie', @$qr->imie, null, TRUE );
            echo table_row_generate( "Nazwisko", "nazwisko", @$qr->nazwisko, null, TRUE );
            echo table_row_separator();
            
            echo table_row_generate( "Firma", "firma", @$qr->firma, null, false, false, @$szablon->firma );
            echo table_row_generate( "Stanowisko", "stanowisko", @$qr->stanowisko, "large_input" );
            echo table_row_separator();
            
            echo table_row_generate( "Adres (ulica, nr)", "ulica", @$qr->ulica, null, false, false, @$szablon->ulica );
            echo table_row_generate( "Miejscowość", "miasto", @$qr->miasto, null, false, false, @$szablon->miasto );
            echo table_row_generate( "Kod pocztowy", "kod_pocztowy", @$qr->kod_pocztowy, "smallest_input", false, false, @$szablon->kod_pocztowy );
            echo table_row_generate( "Kraj", "kraj", @$qr->kraj, "small_input", false, false, @$szablon->kraj );
            echo table_row_separator();
            
            echo table_row_generate( "Telefon stacjonarny", "stacjonarny", @$qr->stacjonarny, null, false, false, @$szablon->stacjonarny );
            echo table_row_generate( "Telefon komorkowy", "komorkowy", @$qr->komorkowy, null, false, false, @$szablon->komorkowy);
            echo table_row_generate( "Fax", "fax", @$qr->fax, null, false, false, @$szablon->fax);
            echo table_row_separator();
            
            echo table_row_generate( "Adres strony www", "url", @$qr->url, null, false, false, @$szablon->url );
            echo table_row_generate( "Adres e-mail", "email", @$qr->email, "large_input" );
            echo table_row_separator();
        ?>
        
        <tr>
            <td class="label">
                <input type="submit"class="przycisk_generuj" id="nowy_kod_qr"
                    value="Generuj .png i .EPS">
            </td>
            <td class="buttons">
                <div class="button" id="zapisz_kod_qr">Zapisz kontakt</div>
                <?php if(@$qr->qr_id):?>
                <br /><div class="button" id="zapisz_nowy_kod_qr">Zapisz jako <br />nowy kontakt</div>
                <?php endif; ?>
            </td>
        </tr>
    </table>
    <input type="hidden" name="qr_id" id="qr_id" value="<?php echo @$qr->qr_id;?>" >
    <input type="hidden" name="utworzono" id="utworzono" value="<?php  if(@$szablon->utworzono) {echo @$szablon->utworzono;} else {echo date('Y-m-d H:i:s');}?>" >
    <input type="hidden" name="form_action" id="form_action" value="<?php echo @$form_action;?>" >

    
</form>
<div id="wygenerowany_kod" class="margin_top_50">
    <?php if(is_file(@$qr_filepath.'.eps')): ?>
        <?php echo anchor($qr_filepath.'.eps', 'pobierz EPS', 'class="download-button"'); ?>
    <?php endif; ?>
    
    <?php if(is_file(@$qr_filepath.'.png')) {
        echo anchor($qr_filepath.'.png', 'pobierz PNG', 'class="download-button"');
        echo img(@$qr_filepath.'.png');
    } ?>
</div>