<h2>Generowanie nowego wzorca (szablonu)</h2>
<form class="formularz nowy" id="formularz_szablon" method="POST">
    <table class="styled_table">
        <tr>
            <td class="label">Nazwa szablonu</td>
            <td><input name="nazwa" value ="<?php echo @$szablon->nazwa;?>" required></td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        
        <tr>
            <td class="label">Firma</td>
            <td><input name="firma" value ="<?php echo @$szablon->firma;?>" required></td>
        </tr>
        <tr>
            <td class="label">Stanowisko</td>
            <td><input name="stanowisko" value ="<?php echo @$szablon->stanowisko;?>" ></td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        
        <tr>
            <td class="label">Adres (Ulica, nr)</td>
            <td><input name="ulica" value ="<?php echo @$szablon->ulica;?>" ></td>
        </tr>
        <tr>
            <td class="label">Miejscowość</td>
            <td><input name="miasto" value ="<?php echo @$szablon->miasto;?>" ></td>
        </tr>
        <tr>
            <td class="label">Kod pocztowy</td>
            <td><input name="kod_pocztowy" value ="<?php echo @$szablon->kod_pocztowy;?>" class="smallest_input"></td>
        </tr>
        <tr>
            <td class="label">Kraj</td>
            <td><input name="kraj" value ="<?php echo @$szablon->kraj;?>" class="small_input"></td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        
        <tr>
            <td class="label">Telefon stacjonarny</td>
            <td><input name="stacjonarny" value ="<?php echo @$szablon->stacjonarny;?>" ></td>
        </tr>
        <tr>
            <td class="label">Telefon komórkowy</td>
            <td><input name="komorkowy" value ="<?php echo @$szablon->komorkowy;?>" ></td>
        </tr>
        <tr>
            <td class="label">Fax</td>
            <td><input name="fax" value ="<?php echo @$szablon->fax;?>" ></td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        
        <tr>
            <td class="label">Adres strony www</td>
            <td><input name="url" value ="<?php echo @$szablon->url;?>" ></td>
        </tr>
        <tr>
            <td class="label">Adres e-mail</td>
            <td><input name="email" value ="<?php echo @$szablon->email;?>" ></td>
        </tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr>
            <td class="label">&nbsp;</td>
            <td><input type="submit" value="Zapisz wzorzec" id="zapisz_szablon"></td>
            
            <input type="hidden" name="szablon_id" value="<?php echo @$szablon->szablon_id;?>" >
            <input type="hidden" name="utworzono" value="<?php  if(@$szablon->utworzono) {echo @$szablon->utworzono;} else {echo date('Y-m-d H:i:s');}?>" >
            <input type="hidden" name="form_action" id="form_action" value="<?php echo @$form_action;?>" >
        </tr>
    </table>

    
    
</form>