<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('functions'))
{
    function table_row_generate(    
            $label = null,      $name = null,       $value = null,      $class = null, 
            $required = false,  $disabled = false,  $hi_variable = null )
    {
        if(($hi_variable))
            $val = $hi_variable;
        else
            $val = $value;
        $val = trim($val);
        
        
        $options = "";
        if( $required === true )  $options .= "required ";
        if( $disabled === true OR $hi_variable)  $options .= "readonly ";
        if( $class ) $options .= 'class="'.$class.'"';
        

        
        $ret = '<tr>
            <td class="label">'.$label.'</td>
            <td><input name="'.$name.'" value ="'.$val.'" '.$options.'></td>
        </tr>';
        
        return $ret;
    }
    
    
    function table_row_separator()
    {
        return '<tr><td colspan="2">&nbsp;</td></tr>';
    }
    
    
    function table_rows_generate ( $lista = array() )
    {
        $output = "";
        foreach ($lista as $l)
            $output .= table_row_generate ($l);
        return $output;
    }
    
    
    function last_added_link ( $imie_nazwisko = null, $qr_id, $firma )
    {
        /* Jeśli imie nazwisko ma wiecej niz 39 znakow, skracamy
         *   Jesli jest firma
         *      jesli [dlugosc imie nazwisko] > 29 znakow       (10 znakow na firme i 3litery w nawiasie)
         * 
         */
        
        return false;
    }
}