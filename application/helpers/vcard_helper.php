<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('vcard'))
{
    function vCard( $dane = array() )
    {
        $data = "BEGIN:VCARD\n".
        "VERSION:2.1\n".
        "N:" .$dane['nazwisko'].';'.$dane['imie']."\n";
        "FN:".$dane['imie'].' '.$dane['nazwisko']."\n";

        if(@$dane['firma'])		$data .= "ORG:".			$dane['firma']."\n";
        if(@$dane['stanowisko'])	$data .= "TITLE:".                      $dane['stanowisko']."\n";

        if(@$dane['ulica'] || @$dane['miejscowosc'] || @$dane['kod_pocztowy'])
            $data .= "ADR;WORK:;;".$dane['ulica'].";".$dane['miasto'].";;".$dane['kod_pocztowy'].";".$dane['kraj']."\n";

        if(@$dane['stacjonarny'])	$data .= "TEL;WORK:".                   $dane['stacjonarny']."\n";
        if(@$dane['fax'])               $data .= "TEL;FAX:".                    $dane['fax']."\n";
        if(@$dane['komorkowy'])		$data .= "TEL;CELL:".                   $dane['komorkowy']."\n";
        
        if(@$dane['email'])		$data .= "EMAIL;INTERNET;WORK:".	$dane['email']."\n";
        if(@$dane['url'])		$data .= "URL,WORK:".                        $dane['url']."\n";

        $data .= "END:VCARD";
        
        
        return $data;
    }
    
    
    function vCard3( $dane = array() )
    {
        $data = "BEGIN:VCARD\n".
        "VERSION:3.0\n".
        "FN:".$dane['imie'].' '.$dane['nazwisko']."\n".
        "N:" .$dane['nazwisko'].';'.$dane['imie']."\n";

        if(@$dane['firma'])		$data .= "ORG:".			$dane['firma']."\n";
        if(@$dane['stanowisko'])	$data .= "TITLE:".			$dane['stanowisko']."\n";

        if(@$dane['ulica'] || @$dane['miejscowosc'] || @$dane['kod_pocztowy'])
            $data .= "ADR;TYPE=WORK:;;".$dane['ulica'].";".$dane['miasto'].";;".$dane['kod_pocztowy'].";".$dane['kraj']."\n";

        if(@$dane['stacjonarny'])	$data .= "TEL;TYPE=WORK,VOICE:".	$dane['stacjonarny']."\n";
        if(@$dane['fax'])               $data .= "TEL;TYPE=WORK,FAX:".          $dane['fax']."\n";
        if(@$dane['komorkowy'])		$data .= "TEL;TYPE=CELL:".		$dane['komorkowy']."\n";
        
        if(@$dane['email'])		$data .= "EMAIL;PREF;INTERNET:".	$dane['email']."\n";
        if(@$dane['url'])		$data .= "URL:".                        $dane['url']."\n";

        $data .= "END:VCARD";
        
        
        return $data;
    }
    function qr_image ( $string, $size_mm = "10", $correction = 0, $margin = 0) {
        
        if( $string ) {
            // KOREKCJA BŁĘDÓW
            if( $correction < 4 && $correction > 0) {
                $c_table = array("L", "M", "Q", "H");
                $clevel = $c_table[$correction];
            }
            else $clevel = "L";
            
            // ROZMIAR w mm
            $size = round($size_mm/10/2.54*300);
            if      ($size > 540) $size = 540;
            elseif  ($size < 100) $size = 200;  
            
            $ret  = '<img src="http://chart.apis.google.com/chart';
            $ret .= '?cht=qr';                      // typ - QR
            $ret .= '&chs='.$size.'x'.$size;        // rozmiar w px (przeliczenie z mm
            $ret .= '&chld='.$clevel.'|'.$margin;   // korekcja (jeśli istnieje) i margines (4)
                
            $ret .= '&chl='.urlencode($string);     // dane kodu
            $ret .= '" />';
            
            // tylko sprawdzajaca
            
            
            return $ret;
        }
        else
            return false;
    }
}