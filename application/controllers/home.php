<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
        
        public function __construct()
        {
             parent::__construct();
             $this->load->model('model_qr');
             $this->load->model('model_szablon');
             $this->load->helper('html');
             $this->load->library('ciqrcode');
             
             define('QRPATH',"images/qrcodes/");
        }
        
        private function generuj_widok( $nazwa_widoku = "", $zmienne = array() )
        {
        // ladowanie elementów stałych
            $zasoby['styl'] = array (
                base_url().'css/960/960_24_col.css',
                base_url().'css/960/reset.css',
                base_url().'css/960/text.css',
                base_url().'css/styl.css',
                );
            $zasoby['skrypt'] = array(
                base_url().'js/jquery.js',
                base_url().'js/jquery.dataTables.min.js',
                base_url().'js/skrypty_ajax.js'
                );
            
        // powtarzalne zmienne
            $zmienneZbazy['ostatnio_dodane']    = $this->model_qr->ostatnio_dodane();
            $zmienneZbazy['ile_kodow']          = $this->model_qr->ile_kodow();
            $zmienneZbazy['ile_szablonow']      = $this->model_szablon->ile_szablonow();
            
        // łączenie danych DATA i DANE oraz zmiennych w jedną tablicę
            $zmienne_razem = @array_merge($zmienneZbazy, $zmienne);
            $zmienne = @array_merge($zmienne_razem, $zasoby);
            
        // generowanie widoku z nagłówkiem i stopką
            $this->load->view('base_header', @$zmienne);
            $this->load->view($nazwa_widoku);
            $this->load->view('base_footer');
        }
        
        
        
        

	public function index( $przesuniecie = 0, $limit = 15 ) {
            // PAGINACJA
            $this->load->library('pagination');
            $conf = array(
                'base_url'      => site_url('home/index'),
                'total_rows'    => $this->model_qr->ile_kodow(),
                'per_page'      => $limit,
                'full_tag_open' => '<div class="pagination">',
                'full_tag_close'=> '</div>'
            );
            $this->pagination->initialize($conf);
            $data['paginacja']          = $this->pagination->create_links();
            $data['lista']              = $this->model_qr->pobierz_liste( $limit, $przesuniecie );
            
            $this->generuj_widok('widok_home_lista', $data);
	}
    
        
	public function create()
	{
            $data['szablony_radio'] = $this->model_szablon->pobierz_liste_radio();
            $this->generuj_widok('widok_kod', $data);
	}
        
        
        public function view( $id = null, $szablon = null )
        {   
            // pobiera dane dla danego QRa
            $kod = $this->model_qr->pobierz( $id );
            
            
            if(!@$kod) // sprawdza, czy kod jest w bazie, jeśi jest wyświetla, jeśli nie, przekierowuje do głównej
                redirect ('home/index');
            else
            { 
                $data['qr'] = $kod; // przekazuje dane do zmiennej DATA
                
                // pobiera dane z szablonu, jeśli istnieją, przekazuje do DATA
                if($szablon) // jesli wymuszony podgląd szablonu
                { 
                    $dane_szablon = $this->model_szablon->pobierz($szablon);
                    if($dane_szablon) $data['szablon'] = $dane_szablon;
                }
                elseif($kod->szablon_id AND $kod->szablon_id!=0)  // jesli brak podgladu, ale szablon istnieje
                {
                    $dane_szablon = $this->model_szablon->pobierz($kod->szablon_id);
                    if($dane_szablon) $data['szablon'] = $dane_szablon;
                }   
                //pobiera liste szablonów do wyświetlenia, przekazuje do DATA
                $data['szablony_radio'] = $this->model_szablon->pobierz_liste_radio();

                // dodaje akcję EDYCJA, jeśli jej nie ma (kod jest wyświetlany, nie był jeszcze edytowany)
                if(!@$data['form_action']) $data['form_action'] = 'edit';
         
                // stala nazwa dla plikow QR (png, eps)
                $data['qr_filepath'] = QRPATH.'qr-'.$id;
                        
                $this->generuj_widok('widok_kod', $data);
            }
        }
        
        
        public function szablony()
	{
            $data['lista'] = $this->model_szablon->pobierz_liste_szablonow( );
            $this->generuj_widok('widok_szablony', $data);
	}
        
        
        public function szablon( $id = null )
        {
            //$this->output->enable_profiler(TRUE);
            if($id)
            {
                $szablon = $this->model_szablon->pobierz($id);
                if(!$szablon)
                    redirect('home/szablony');
                else
                {
                    $data['form_action'] = 'edit';
                    $data['szablon'] = $szablon;
                }
            }
            else
            {
                $data['form_action'] = 'create';
            }
            $this->generuj_widok('widok_szablon', $data); 
        }    
        
        
        
/* SAM AJAX */
        
        public function ajax_generuj() // stara funkcja, już nie używana
        {
            if( $this->input->is_ajax_request() )
            {
                $data['dane'] = qr_image( vCard3($this->input->post()), 35 );
                $this->load->view('widok_ajax_kod', @$data);
            }
        }
        public function ajax_generuj2($correction = 0)
        {
            if( $this->input->is_ajax_request() )
            {
                $qr_id = $this->input->post('qr_id');
                
                $correction_levels = array("L", "M", "Q", "H");
                if($correction>3 OR $correction < 1 OR !is_numeric($correction)) $correction = 0;
                
                // generowanie QR-kodów
                $filename = 'qr-'.$qr_id;
                $code['data']   = vCard3($this->input->post());
                $code['level']  = $correction_levels[$correction];
                $code['size']   = 7;
                //$code['white']  = array(45, 62, 80);
                
                // zapisuje PNG
                $code['type'] = 'png';
                $code['savename'] = QRPATH.$filename.'.'.$code['type'];
                $this->ciqrcode->generate($code);
                
                // zapisuje EPS
                $code['type'] = 'eps';
                $code['savename'] = QRPATH.$filename.'.'.$code['type'];
                $this->ciqrcode->generate($code);
                
                // zapisuje SVG
                
                
                //$data['dane'] = qr_image( vCard3($this->input->post()), 35 );
                $data['dane'] = 
                        anchor(QRPATH.$filename.'.eps', 'pobierz EPS', 'class="download-button"').
                        anchor(QRPATH.$filename.'.png', 'pobierz PNG', 'class="download-button"').
                        img(QRPATH.$filename.'.png');
                $this->load->view('widok_ajax_kod', @$data);
            }
        }
        public function ajax_zapisz()
        {
            if( $this->input->is_ajax_request() ) {                
                $kod = $this->input->post();

                $res = $this->model_qr->zapisz( $kod );
                if($res) $data['redirect'] = site_url('home/view/'.$res);
                $this->load->view('widok_ajax_stan', @$data);
            }
        }
        
        
        public function ajax_zapisz_szablon()
        {
            if( $this->input->is_ajax_request() ) {
                $szablon = $this->input->post();

                $res = $this->model_szablon->zapisz( $szablon );
                if($res) $data['redirect'] = /*site_url('home/szablon/'.$res);*/ site_url('home/szablony');
                $this->load->view('widok_ajax_stan', @$data);
            }
        }
        
/* tylko testujące */

}
