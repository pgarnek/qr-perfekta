<?php

    class Model_szablon extends CI_Model {

         function __construct() {
            parent::__construct();
         }
         
         
/*
 *  SZABLONY
 */       
         
         function pobierz ($id = null)
         {
             $this->db->where('szablon_id', $id);
             $query = $this->db->get('szablon');
             
             if($query)
                 return $query->row();
             else
                 return false;
         }
         
         function zapisz( $szablon = array() ) {
             //wywalenie niepotrzebnych pol formularza
             unset( $szablon['form_action'] );
             
             
             // WARUNEK: sprawdzenie, czy kod jest zapisywany od nowa, czy nadpisywany...
             if( $szablon['szablon_id']) {
                 //uaktualniany
                 $this->db->where('szablon_id', $szablon['szablon_id']);
                 if( $this->db->update( 'szablon', $szablon ) )
                    return $szablon['szablon_id'];
             }
             else {
                 //tworzony
                 if( $this->db->insert( 'szablon', $szablon ) )
                     return $this->db->insert_id();  
             }
                
             if( !$res )
                 return false;
         }
         
         function pobierz_liste_szablonow( /*$limit = 30, $przesuniecie = 0*/ ) {

             $this->db->select('szablon_id, nazwa,firma, utworzono, zmodyfikowano');
             $this->db->order_by("zmodyfikowano", "desc");
             $query = $this->db->get('szablon');
             return $query->result();
         }
         
         
         function pobierz_liste_radio()
         {
             $this->db->select( 'szablon_id, nazwa' );
             $this->db->order_by('nazwa');             
             $query = $this->db->get('szablon');
             
             $lista_query = $query->result();
             
             
             $lista_radio[] = '( brak )';
             foreach ($lista_query as $q)
                 $lista_radio[$q->szablon_id] = $q->nazwa;
             
             return $lista_radio;
         }
         
         function ile_szablonow() {
             return $this->db->count_all('szablon');
         }
         
         
    }