<?php

    class Model_qr extends CI_Model {

         function __construct() {
            parent::__construct();
         }
         
         function pobierz( $id = null ) {
             $this->db->where('qr_id', $id);
             $query = $this->db->get('qr');
             
             if($query)
                 return $query->row();
             else
                 return false;
         }
         function ostatni() {
             $this->db->select('qr_id');
             $query = $this->db->get('qr', 1);
             $result = $query->result_array();
             return $result[0]['qr_id'];
         }
         
         function zapisz( $kod = array() ) {
             //wywalenie niepotrzebnych pol formularza
             unset( $kod['form_action'] );
             
             
             // WARUNEK: sprawdzenie, czy kod jest zapisywany od nowa, czy nadpisywany...
             if( $kod['qr_id']) {
                 //uaktualniany
                 $this->db->where('qr_id', $kod['qr_id']);
                 if( $this->db->update( 'qr', $kod ) )
                    return $kod['qr_id'];
             }
             else {
                 //tworzony
                 if( $this->db->insert( 'qr', $kod ) )
                     return $this->db->insert_id();  
             }
                
             if( !$res )
                 return false;
         }
         
         
         function pobierz_liste( $limit = 30, $przesuniecie = 0) {
             $this->db->limit( $limit, $przesuniecie );
             $this->db->order_by("zmodyfikowano", "desc"); 
             $query = $this->db->get('qr');
             return $query->result();
         }
         
         function ostatnio_dodane( $limit = 5 ) {
             $lista = $this->pobierz_liste( $limit );
             foreach($lista as $l) {
                 $l_nazwa = $l->imie." ".$l->nazwisko;
                 if($l->firma) $l_nazwa .= " ( ".$l->firma." )";
                 $ostatnie[] = array(
                     'id'       => $l->qr_id,
                     'nazwa'    => $l_nazwa
                 );
             }
             return $ostatnie;
         }
         
         function ile_kodow() {
             return $this->db->count_all('qr');
         }
         /*         
         function szukaj( $fraza ) {
             $this->select_krotki();
             $this->db->distinct();
             $this->db->like( 'sap.sap', $fraza);
             $this->db->or_like( 'sap.nazwa', $fraza);
             $query = $this->db->get('sap');
             
             return $query->result_array();
         }*/
           
         
    }